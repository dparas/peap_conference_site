<?php
session_start();
$_SESSION = array();

include("captcha.php");
$_SESSION['captcha'] = captcha();

?>
<html xmlns="http://www.w3.org/1999/xhtml">

    <head>
       <title></title>
       <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
       <!--<script language="javascript" type="text/javascript" src="Niceforms-master/niceforms.js"></script>-->
       <!--<link rel="stylesheet" type="text/css" media="all" href="Niceforms-master/niceforms-default.css" />-->
       <link rel="stylesheet" type="text/css" media="all" href="form.css" />
       <link rel="stylesheet" type="text/css" media="all" href="validation/css/validationEngine.jquery.css" />
       <!--<script type="text/javascript" src="jquery.js"></script>
       <script type="text/javascript" src="js.js"></script>-->
       <script type="text/javascript" src="validation/js/jquery-1.8.2.min.js"></script>
       <script type="text/javascript" src="validation/js/languages/jquery.validationEngine-en.js"></script>
       <script type="text/javascript" src="validation/js/jquery.validationEngine.js"></script>
               
       
       <script>
		jQuery(document).ready(function(){
			// binds form submission and fields to the validation engine
			jQuery("#conferenceform").validationEngine();
		});
            
	</script>
       
    </head>

    <body>
        
        <div id="title">
            1o Ελληνικό Συνέδριο με Θέμα<br>
                <b>ΤΑ ΠΡΩΤΑ ΒΗΜΑΤΑ ΠΡΟΣ ΤΗΝ ΠΟΛΥΓΛΩΣΣΙΑ:</b><br>
                    <b>ΤΟ ΠΡΟΓΡΑΜΜΑ «ΕΚΜΑΘΗΣΗΣ ΤΗΣ ΑΓΓΛΙΚΗΣ ΣΤΗΝ ΠΡΩΙΜΗ ΠΑΙΔΙΚΗ ΗΛΙΚΙΑ»</b><br>
14 - 16 Ιουνίου 2013
        
        </div>
        
        <div id="infoheader">
            <p>
                Η γλώσσα του Συνεδρίου είναι η Αγγλική. Για περισσότερες πληροφορίες: <br>
                    <a href="http://rcel.enl.uoa.gr/peap/conference">http://rcel.enl.uoa.gr/peap/conference</a> <br>

                        <p id="title"><b>ΔΕΛΤΙΟ ΠΡΟΕΓΓΡΑΦΗΣ ΣΥΝΕΔΡΟΥ</b></p>
                        Παρακαλούμε συμπληρώσετε το παρόν Δελτίο Προεγγραφής μέχρι και την <span style="color: #D6000E"><u>30η Μαρτίου 2013</u></span>

                
            </p>
            
            
        </div>
        
       
        
         <form method="post" action="submit.php" class="formular" id="conferenceform">
        
        <div id="contact">
            <div id="top">
                <h1>I.ΑΤΟΜΙΚΑ ΣΤΟΙΧΕΙΑ ΣΥΝΕΔΡΟΥ</h1>
            </div>
            <div id="center">
        <div id="contact_form">
           
                <div class="error" id="error">An error occurer and the message can't be sent!</div>
                <div class="success" id="success">Email sent successfully!<br>Thank you for contacting us.</div>

                    <span class="input">
<label for="name"><b>Όνομα:*</b> </label>
<input type="text" id="name" name="name" class="validate[required] text-input">

</span>
                
                <span class="input">
<label for="name"><b>Επώνυμο:*</b> </label>
<input type="text" id="surname" name="surname" class="validate[required] text-input">

</span>
                
                <span class="input">
<label for="name"><b>Διεύθυνση:*</b> </label>
<input type="text" id="address" name="address" class="validate[required] text-input">
</span>
                
                <span class="input">
<label for="name"><b>Πόλη:*</b> </label>
<input type="text" id="city" name="city" class="validate[required] text-input">
</span>
                
                <span class="input">
<label for="name"><b>Τ.Κ.:*</b> </label>
<input type="postcode" id="postcode" name="postcode" class="validate[required, custom[onlyNumberSp]] text-input">
</span>
                
                <span class="input">
<label for="name"><b>Τηλέφωνο:*</b> </label>
<input type="text" id="phone" name="phone" class="validate[required, custom[onlyNumberSp]] text-input">
</span>

<span class="input">
<label for="email"><b>E-mail:*</b> </label>
<input type="text" id="email" name="email" class="validate[required, custom[email]] text-input">
</span>

<span class="input">
<label for="phone"><b>Κινητό Τηλ.:*</b> </label>
<input type="text" id="mobile" name="mobile" class="validate[required, custom[onlyNumberSp]] text-input">
</span>


<span class="input">
<label for="sales"><b>Fax:</b> </label>
<input type="text" id="fax" name="fax" class="validate[required, custom[onlyNumberSp]] text-input">
</span>

<span class="input">
    Σε περίπτωση που θέλετε να παρουσιάσετε την εργασία σας σε poster session, συμπληρώστε το θέμα σας
<label for="message"> </label>
<textarea id="poster" name="poster" style="width: 400; height: 140;"">
</textarea>
</span>
                
                <span class="input">
    Για να απαλλαχθείτε από το κόστος της εγγραφής σας στο συνέδριο πρέπει να διδάσκετε σε δημόσιο δημοτικό. Συμπλρώστε τα στοιχεία σας.
<label for="message"> </label>
<textarea id="message" name="message" style="width: 400; height: 140;" >
</textarea>
<div class="warning" id="messageError">This field can't stay empty</div>
</span>

    <span class="input">
        <br> Σε περίπτωση που θέλετε να παρακολουθήσετε το Συνέδριο και <b><u>δεν</u></b> είστε εκπαιδευτικός Αγγλικής σε δημόσιο Δημοτικό, μάθετε περισσότερες πληροφορίες εδώ:
        
    </span>

                </div>
            </div>
            <div id="bot"></div>
  
        </div>
        
        <div id="contact">
            <div id="top">
                <h1>ΙΙ.ΔΙΑΔΙΚΑΣΙΑ & ΧΡΟΝΟΙ ΕΓΓΡΑΦΗΣ</h1>
            </div>
            <div id="center">
                <div id="info" style="text-align: justify">
                    <p>
                    <b>Μετά τις 30 Μαΐου 2013</b>, εγγραφές θα γίνονται δεκτές μόνο στη Γραμματεία του συνεδρίου, η οποία θα λειτουργεί στο συνεδριακό κέντρο, την ημέρα έναρξης του συνεδρίου (<u>με την προϋπόθεση ότι θα υπάρχουν διαθέσιμες θέσεις</u>).
</br></br>
Σας ενημερώνουμε ότι τα δεδομένα προσωπικού χαρακτήρα, που συλλέγονται από το παρόν Δελτίο Προεγγραφής Συνέδρου, χρησιμοποιούνται αποκλειστικά για την εκπλήρωση των σκοπών της εγγραφής σας στο Συνέδριο. Σε καμία περίπτωση δεν πωλούνται, ενοικιάζονται ή καθ’ οιονδήποτε τρόπο διαβιβάζονται ή/και κοινοποιούνται σε τρίτους. Η δε επεξεργασία και διαχείρισή τους υπόκειται στις σχετικές διατάξεις της ελληνικής νομοθεσίας και του ευρωπαϊκού δικαίου για την προστασία του ατόμου και την προστασία δεδομένων προσωπικού χαρακτήρα.
                </br></br>
Εντός δύο (2) ημερών από τη λήψη του Δελτίου Προεγγραφής σας, θα σας αποσταλεί επιβεβαίωση προεγγραφής μέσω e-mail. Σε περίπτωση που δε λάβετε τη σχετική επιβεβαίωση, παρακαλούμε όπως επικοινωνήσετε με την εταιρεία διοργάνωσης του συνεδρίου/ Γραφείο ΠΕΑΠ.
</br></br>
Στο συνέδριο θα μοιραστεί εκπαιδευτικό υλικό και επιστημονικά έντυπα. Μετά το πέρας του Συνεδρίου, όλοι οι εγγεγραμμένοι σύνεδροι θα λάβουν Πιστοποιητικό Παρακολούθησης.


</p></div>
                
        
            </div>
            <div id="bot"></div>
  
        </div>
        
       
        <div id="contact">
            <div id="top">
                <h1>III.ΑΠΟΣΤΟΛΗ</h1>
            </div>
            <div id="center">
        <div id="contact_form">
            <center><span class="input"> <label for="security_code">Κωδικός Ασφάλειας:</label>
<input class="validate[required] text-input"  type="text" id="security_code" name="security_code" style="width:80px">
</br>
</br>

    <?php
        	echo '<img src="' . $_SESSION['captcha']['image_src'] . '" alt="CAPTCHA" />';
        	
                
               // $_POST['session_code']=$_SESSION['captcha']['code']; ?>

</span>
</center>
                    


    <br>
    
                    <span id="submit" class="input">
                    <label for="submit"></label>
                    <p id="ajax_loader" style="text-align:center;"><img src="assets/img/contact/ajax-loader.gif"></p>
                    <input id="send" type="submit" value="ΥΠΟΒΟΛΗ">
                    </span>
        
        
                </div>
            </div>
            <div id="bot"></div>
  
        </div>
         </form>
        
        </body>
    
</html>

