<html>
<head>
<title>PHPMailer - SMTP basic test with authentication</title>
</head>
<body>

<?php

//error_reporting(E_ALL);
error_reporting(E_STRICT);

date_default_timezone_set('Europe/Athens');

require_once('../class.phpmailer.php');
//include("class.smtp.php"); // optional, gets called from within class.phpmailer.php if not already loaded

$mail             = new PHPMailer();

//$body             = file_get_contents('contents.html');
//$body             = preg_replace('/[\]/','',$body);

$body = "test";

$mail->IsSMTP(); // telling the class to use SMTP
$mail->Host       = "smtp.uoa.gr"; // SMTP server
$mail->SMTPDebug  = 2;                     // enables SMTP debug information (for testing)
                                           // 1 = errors and messages
                                           // 2 = messages only
$mail->SMTPAuth   = true;                  // enable SMTP authentication
//$mail->Host       = "mail.yourdomain.com"; // sets the SMTP server
$mail->SMTPSecure = "ssl";
$mail->Port       = 465;                    // set the SMTP port for the GMAIL server
$mail->Username   = "dparas"; // SMTP account username
$mail->Password   = "1eswtwc";        // SMTP account password

$mail->SetFrom('dparas@enl.uoa.gr', 'Dimitris Paras');

$mail->AddReplyTo("dparas@enl.uoa.gr","Dimitris Paras");

$mail->Subject    = "PHPMailer Test Subject via smtp, basic with authentication";

$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test

$mail->MsgHTML($body);

$address = "dimitris_paras@yahoo.gr";
$mail->AddAddress($address, "John Doe");

//$mail->AddAttachment("images/phpmailer.gif");      // attachment
//$mail->AddAttachment("images/phpmailer_mini.gif"); // attachment

if(!$mail->Send()) {
  echo "Mailer Error: " . $mail->ErrorInfo;
} else {
  echo "Message sent!";
}

?>

</body>
</html>
